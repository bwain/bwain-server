// Librerías
var express = require('express') // Libreria express
var bodyParser = require('body-parser') // Librería body-parser
var requestJson = require('request-json') // Librería request-json

// Variables para construir las llamadas a mLab
var urlMlabRaiz = 'https://api.mlab.com/api/1/databases/bwain/collections' //  bwain-server URL mLab
var apiKey = 'apiKey=OM4bFN8N0-jAr8cwT61SlKco4IXq_N6n' // bwain-server apiKey mLab

// Configuración y puesta en marcha del servidor
var port = process.env.PORT || 3000 // Puerto del servidor
var app = express() // Servidor Node.js
app.use(bodyParser.json()) // Leer el body con formato json
app.listen(port) // Levantar la aplicacion en el puerto definido
console.log("BWAIN Server escuchando con atención en el puerto " + port) // Mensaje del servidor

/**************************
 *  API REST bwain-server *
 **************************/

// Bienvenido a BWAIN Server
app.get('/bwain-server/v1', function(req, res) {
  res.status(200).send({"mensaje": "Bienvenido a BWAIN Server"})
})

/****************
 * API Usuarios *
 ****************/

// Obtiene todos los registros de collection usuarios de mLab en un array
// A esta API sólo debería tener acceso el Admin
app.get('/bwain-server/v1/usuarios', function(req, res) {
  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err && body.length > 0) {
      res.status(200).send(body)
    } else {
      res.status(404).send({"operación": "GET /usuarios", "detalle": "Error de mLab o no hay usuarios"})
    }
  })
})

// Dado el id de un usuario, se obtienen todos sus datos
// pasamos el id por cabecera (headers)
app.get('/bwain-server/v1/usuarios/clientes', function(req, res) {
  var id = req.headers.id // Obtebenemos el id del usuario
  var query = 'q={"id":' + id + '}&f={"_id":0}' // Construimos la consulta para mLab
  var urlMlab = urlMlabRaiz + '/usuarios?' + query + '&' + apiKey // Construimos la URL completa de mLab
  var clienteMlab = requestJson.createClient(urlMlab) // Llamamos a mLab
  clienteMlab.get ('', function(err, resM, body) {
    if (!err && body.length === 1 && !body[0].deleted) { // Comprobamos que no hay error, que el usuario existe y que no ha sido borrado
      res.status(200).send(body)
    } else {
      res.status(404).send({"operación": "GET /usuarios/clientes", "detalle": "Usuario no encontrado", "id": id}) // Usuario no encontrado
    }
  })
})

// Dado el id de un usuario, se obtienen todos sus datos
// pasamos el id por parámetro (params)
app.get('/bwain-server/v1/usuarios/:id', function(req, res) {
  var id = req.params.id // Obtebenemos el id del usuario
  var query = 'q={"id":' + id + '}&f={"_id":0}' // Construimos la consulta para mLab
  var urlMlab = urlMlabRaiz + '/usuarios?' + query + '&' + apiKey // Construimos la URL completa de mLab
  var clienteMlab = requestJson.createClient(urlMlab) // Llamamos a mLab
  clienteMlab.get ('', function(err, resM, body) {
    if (!err && body.length === 1 && !body[0].deleted) { // Comprobamos que no hay error, que el usuario existe y que no ha sido borrado
      res.status(200).send(body)
    } else {
      res.status(404).send({"operación": "GET /usuarios/:id", "detalle": "Usuario no encontrado", "id": id}) // Usuario no encontrado
    }
  })
})

// Login si coincide el email y el password. Al usuario se le añade el atributo logged=true
app.post('/bwain-server/v1/login', function(req, res) {
  var email = req.headers.email // Obtenemos el e-mail del usaurio
  var password = req.headers.password // Obtenemos la contraseña del usuario
  var query = 'q={$and:[{"email":"' + email + '"},{"password":"' + password + '"}]}' // Construimos la consulta para mLab
  var urlMlab = urlMlabRaiz + '/usuarios?' + query + '&' + apiKey // Construimos la URL completa de mLab
  var clienteMlab = requestJson.createClient(urlMlab) // Llamamos a mLab
  clienteMlab.get('', function(err, resM, body) {
    if (!err && body.length === 1 && !body[0].deleted) { // Si no hay error, el usuario existe y no está dado de baja
      var cambio = '{"$set":{"logged":true}}' // Preparamos el cambio del atributo logged
      clienteMlab.put('', JSON.parse(cambio), function(errP, resP, bodyP) {
        if (!errP) {
          res.status(200).send({"operacion": "POST /login", "detalle": "El usuario ha iniciado sesión", "id": body[0].id, "nombre": body[0].first_name, "apellidos": body[0].last_name})
        } else {
          res.status(409).send({"operacion": "POST /login", "detalle": "Error al iniciar sesión", "id": body[0].id, "nombre": body[0].first_name, "apellidos": body[0].last_name})
        }
      })
    } else {
      res.status(404).send({"operacion": "POST /login", "detalle": "Email y/o contraseña incorrectos"})
    }
  })
})

// Logout. Finalizar la sesión del usaurio autenticado (logado) mediante el id del cliente
app.post('/bwain-server/v1/logout', function(req, res) {
  var id = req.headers.id // Obtenemos el id del usuario de la cabecera
  var query = 'q={"id":' + id + ', "logged":true}' // Construimos la consulta para mLab
  var urlMlab = urlMlabRaiz + '/usuarios?' + query + '&' + apiKey // Construimos la llamada a mLab
  var clienteMlab = requestJson.createClient(urlMlab) // Llamamos a la API de mLab
  clienteMlab.get('', function(err, resM, body) { // Obtenemos los resultados
    if (!err && body.length === 1 && body[0].logged) { // Si no hay error y el id del usuario existe y ha iniciado sesión
      var cambio = '{"$set":{"logged":false}}' // Preparamos el cambio del atributo logged del usuario
      clienteMlab.put('', JSON.parse(cambio), function(errP, resP, bodyP) {
        if (!errP) {
          res.status(200).send({"operacion": "POST /logout", "detalle": "El usuario ha finalizado sesión", "id": body[0].id})
        } else {
          res.status(409).send({"operacion": "POST /logout", "detalle": "Error al finalizar sesión", "id": body[0].id})
        }
      })
    } else { // En caso de error asumimos que el id del usuario no existe y enviamos mensaje de error
      res.status(404).send({"operacion": "POST /logout", "detalle": "El usuario no ha iniciado sesión", "id": id})
    }
  })
})

/**
Alta de un nuevo usuario: POST /usuarios
  código 201 = nuevo usuario dado de alta (condición necesaria: el e-mail no existe)
  código 403 = usuario ya existe (condición necesaria: el e-mail existe y el usuario no está dado de baja)
  código 202 = usuario ya existe pero es baja lógica (condición necesaria: el e-mail existe y el usuario está dado de baja lógica) [Por si queremos reactivar un usuario]
  código 409 = otros errores (error al llamar a mLab)
*/
app.post('/bwain-server/v1/usuarios', function(req, res) {
  var first_name = req.headers.first_name
  var last_name = req.headers.last_name
  var email = req.headers.email
  var gender = req.headers.gender
  var country = req.headers.country
  var password = req.headers.password
  var ip = req.headers['x-forwarded-for'] || req.headers['X-Forwarded-For'] || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress || req.ip
  var id = 0 // Inicializamos el identificador del futuro usuario
  var newUser = {} // Declaramos objeto para guardar los datos del usuario nuevo

  var query = 'q={"email":"' + email + '"}' // Consulta para cotejar si el e-mail existe

  var clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?f={"_id":0,"id":1}&' + apiKey) // Consultamos los ID de todos los usuarios
  clienteMlab.get('', function(err, resM, body) {
    if (!err) { // Si no hay error
      // Calculamos cuál es el identificador de mayor valor
      var idValues = []; // Definimos un nuevo array
      for (var i = 0; i < body.length; ++i) { // Recorremos el body
        idValues.push(body[i].id) // Generamos un array de números (valores de cada id)
      }
      id = Math.max(...idValues) + 1 // Obtenemos el valor máximo y caculamos el nuevo ID
      // Realizamos consulta para ver si ya existe un usuario con el mismo e-mail
      clienteMlab = requestJson.createClient(urlMlabRaiz + '/usuarios?' + query + '&' + apiKey) // Consultamos si el e-mail ya existe
      clienteMlab.get('', function(err, resM, body) {
        if (!err && body.length === 1 && body[0].deleted) { // Si no hay error, existe el usuario y está marcado como baja lógica
          res.status(202).send({"operación": "POST /usuarios", "detalle": "El usuario ya existe pero está marcado como baja lógica"})
        } else if (!err && body.length === 1) { // Si no hay error y el usuario existe
          res.status(403).send({"operación": "POST /usuarios", "detalle": "No se puede utilizar el e-mail para gestionar su alta. Por favor, elija otro"})
        } else if (!err && body.length === 0) { // Si no hay error y el usuario no existe
          newUser.id = id
          newUser.first_name = first_name
          newUser.last_name = last_name
          newUser.email = email
          newUser.gender = gender
          newUser.ip_address = ip
          newUser.country = country
          newUser.password = password
          // Damos de alta el nuevo usuario en mLab
          clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
          clienteMlab.post('', newUser, function(errP, resP, bodyP) {
            if (!errP) { // Si no hay error damos de alta al nuevo usuario
              res.status(201).send({"operación": "POST /usuarios", "detalle": "Usuario dado de alta", "id": newUser.id})
            } else {
              res.status(409).send({"operación": "POST /usuarios", "detalle": "Error al insertar el usuario", "id": newUser.id})
            }
          })
        } else { // Se ha producido algún error
          res.status(409).send({"operación": "POST /usuarios", "detalle": "Error al consultar los datos"})
        }
      })
    } else { // Se ha producido algún error
      res.status(409).send({"operación": "POST usuarios", "detalle": "Error al acceder"})
    }
  })
})

/* Baja de usuario: [PUT | DELETE] /usuarios
 *  código 200 = usuario dado de baja (condición necesaria: el id del usuario existe y el usuario no está ya eliminado)
 *  código 404 = usuario no encontrado (condición necesaria: el id del usuario no existe)
 *  código 403 = no se puede dar de baja el usuario (condición necesaria: el id de usuario mantiene cuentas abiertas como cliente)
 *  código 409 = otros errores (error al llamar a mLab)
 */

// Baja lógica: PUT /Usuarios
// Marcamos al usuario con un flag (deleted = true) en mLab
app.put('/bwain-server/v1/usuarios', function(req, res) {
  var id = req.headers.id // Obtenemos de la cabecera HTTP el id del usuario
  var query = 'q={"id":' + id + '}&f={"_id":0}' // Construimos la consulta mLab para buscar el usuario según su id
  var urlMlab = urlMlabRaiz + '/usuarios?' + query + '&' + apiKey // Construimos la URL para llamar a mLab
  var clienteMlab = requestJson.createClient(urlMlab) // Llamamos a mLab para que nos devuelva los datos del usuario
  clienteMlab.get('', function(err, resM, body) { // Gestionamos la respuesta de mLab
    if (!err && body.length==1 && !body[0].deleted) { // Si no hay error, el id de usuario existe y no está borrado previamente
      var queryCuentas = 'q={"idcliente":' + id + '}&f={"_id":0,"iban":1,"saldo":1}' // Construimos la consulta mLab para buscar todas las posiciones del usuario
      var urlMlabCuentas = urlMlabRaiz + '/cuentas?' + queryCuentas + '&' + apiKey // Construimos la URL para llamar a mLab
      var clienteMlabCuentas = requestJson.createClient(urlMlabCuentas) // Llamamos a mLab
      clienteMlabCuentas.get('', function(errCuentas, resCuentas, bodyCuentas) {
        if (!errCuentas && bodyCuentas.length === 0) { // Si no hay error y el usaurio no tiene cuentas como cliente
          var cambio = '{"$set":{"deleted":true}}' // Definimos el cambio a realizar: añadir el atributo "deleted": true al usuario
          clienteMlab.put('', JSON.parse(cambio), function(errP, resP, bodyP) {
            if (!errP) { // Si no hay error
              res.status(200).send({"operación": "PUT /usuarios", "detalle": "Borrado lógico del usuario realizado", "id": id, "borrado": true})
            } else { // Si hay error
              res.status(409).send({"operación": "PUT /usuarios", "detalle": "Error mLab", "id": id, "borrado": false})
            }
          })
        } else { // Si el usuario tiene cuentas como cliente no podemos darle de baja
              res.status(403).send({"operación": "PUT /usuarios", "detalle": "El usuario mantiene posiciones activas", "id": id, "borrado": false})
        }
      })
    } else { // En el caso de que el usuario no exista o esté borrado lógicamente
      res.status(404).send({"operacion": "PUT /usuarios", "detalle": "Usuario no encontrado", "id": id})
    }
  })
})

// Baja física: DELETE /usuarios
// Eliminamos todos los datos del usaurio de mLab
app.delete('/bwain-server/v1/usuarios', function(req, res) {
  var id = req.headers.id // Obtenemos de la cabecera HTTP el id del usuario
  var query = 'q={"id":' + id + '}' // Construimos la consulta mLab para buscar el usuario según su id
  var urlMlab = urlMlabRaiz + '/usuarios?' + query + '&' + apiKey // Construimos la URL para llamar a mLab
  var clienteMlab = requestJson.createClient(urlMlab) // Llamamos a mLab para que nos devuelva los datos del usuario
  clienteMlab.get('', function(err, resM, body) { // Gestionamos la respuesta de mLab
    if (!err && body.length === 1) { // Si no hay error, el id de usuario existe y no está borrado previamente
      var queryCuentas = 'q={"idcliente":' + id + '}&f={"_id":0,"iban":1,"saldo":1}' // Construimos la consulta mLab para buscar todas las posiciones del usuario
      var urlMlabCuentas = urlMlabRaiz + '/cuentas?' + queryCuentas + '&' + apiKey // Construimos la URL para llamar a mLab
      var clienteMlabCuentas = requestJson.createClient(urlMlabCuentas) // Llamamos a mLab
      clienteMlabCuentas.get('', function(errCuentas, resCuentas, bodyCuentas) {
        if (!errCuentas && bodyCuentas.length === 0) { // Si no hay error y el usaurio no tiene cuentas como cliente
          var urlMlab = urlMlabRaiz + '/usuarios/' + body[0]._id.$oid + '?' + apiKey // Construimos la URL para llamar a mLab
          var clienteMlab = requestJson.createClient(urlMlab) // Llamamos a mLab
          clienteMlab.delete('', function(errD, resD, bodyD) {
            if (!errD) { // Si no hay error
              res.status(200).send({"operación": "DELETE /usuarios", "detalle": "Borrado físico del usuario realizado", "id": id, "borrado": true})
            } else { // Si hay error
              res.status(409).send({"operación": "DELETE /usuarios", "detalle": "Error mLab", "id": id, "borrado": false})
            }
          })
        } else { // Si el usuario tiene cuentas como cliente no podemos darle de baja
              res.status(403).send({"operación": "DELETE /usuarios", "detalle": "El usuario mantiene posiciones activas", "id": id, "borrado": false})
        }
      })
    } else { // En el caso de que el usuario no exista o esté borrado lógicamente
      res.status(404).send({"operacion": "DELETE /usuarios", "detalle": "Usuario no encontrado", "id": id})
    }
  })
})

/***************
 * API Cuentas *
 ***************/

// Listado de cuentas: GET /cuentas
// Dado un id de cliente devuelve todos los códigos IBAN y sus saldos
app.get('/bwain-server/v1/cuentas', function(req, res) {
  var idcliente = req.headers.idcliente
  var query = 'q={"idcliente":' + idcliente + '}&f={"iban":1,"saldo":1,"_id":0}'
  var urlMlab = urlMlabRaiz + '/cuentas?' + query + '&' + apiKey
  var clienteMlab = requestJson.createClient(urlMlab)
  clienteMlab.get('', function(err, resM, body) {
    if (!err && body.length > 0) {
      res.status(200).send(body)
    } else {
      res.status(404).send({"operación": "GET /cuentas", "detalle": "El cliente no tiene cuentas", "id": idcliente})
    }
  })
})

// Listado de movimientos: GET /cuentas/movimientos
// Dado un IBAN devuelve todos sus movimientos
app.get('/bwain-server/v1/cuentas/movimientos', function(req, res) {
  var iban = req.headers.iban
  var query = 'q={"iban":' + iban + '}&f={"movimientos":1,"_id":0}'
  var urlMlab = urlMlabRaiz + '/cuentas?' + query + '&' + apiKey
  var clienteMlab = requestJson.createClient(urlMlab)
  clienteMlab.get('', function(err, resM, body) {
    if (!err && body.length > 0) {
      res.status(200).send(body)
    } else {
      res.status(404).send({"operación": "GET /cuentas/movimientos", "detalle": "IBAN no encontrado", "iban": iban})
    }
  })
})

// Listado de movimientos financiables: GET /cuentas/moivimientos/financiaciones
// Dado un IBAN devuelve los movimientos con la marca de si son o no financiables
// Son financiables las operaciones de cargo de más de 300 euros que no han sido financiadas anteriormente
app.get('/bwain-server/v1/cuentas/movimientos/financiaciones', function(req, res) {
  var iban = req.headers.iban
  var query = 'q={"iban":' + iban + '}&f={"movimientos":1,"_id":0}'
  var urlMlab = urlMlabRaiz + '/cuentas?' + query + '&' + apiKey
  var clienteMlab = requestJson.createClient(urlMlab)
  clienteMlab.get('', function(err, resM, body) {
    if (!err && body.length > 0) {
      for (var i = 0; i < body[0].movimientos.length; i++) {
        if ((body[0].movimientos[i].importe < -300) && (!body[0].movimientos[i].financiada)) {
          body[0].movimientos[i].financiable = true
        } else {
          body[0].movimientos[i].financiable = false
        }
      }
      res.status(200).send(body)
    } else {
      res.status(404).send({"operacion": "GET /cuentas/movimientos/financiaciones", "detalle": "IBAN no encontrado", "iban": iban})
    }
  })
})

/*
  Financiar un movimiento financiable: POST /cuentas/movimientos/financiaciones

  Dado un Iban y un ID de movimiento:
  Financiar operación.
  --------------------
   Para ello tenemos que:
   - Marcar operación como financiada
   - Insertar nueva operación con el abono en la cuenta
   - Incrementar saldo de la cuenta con el importe de la operación
*/
app.post('/bwain-server/v1/cuentas/movimientos/financiaciones', function(req, res) {
  // Recogemos las cabeceras de la petición (iban e identificador del movimiento)
  var iban = req.headers.iban
  var idMovimiento = parseInt(req.headers.idmovimiento) // Nos aseguramos de que el tipo de dato sea número
  // Variables que necesitamos para financiar un movimiento
  var movimientoNuevo = {} // Objeto del nuevo movimiento
  var fecha = new Date() // Fecha del nuevo movimiento
  var indiceOperacion = 0 // Identificador del nuevo movimiento
  var idValues = [] // Definimos un nuevo array para ayudarnos a calcular el id del nuevo movimiento
  // Variables para interacturar con mLab
  var query = 'q={"iban":' + iban + '}' // Construimos la consulta para mLab
  var urlMlab = urlMlabRaiz + '/cuentas?' + query + '&' + apiKey // Construimos la URL completa de mLab
  var clienteMlab = requestJson.createClient(urlMlab) // Llamamos a mLab
  clienteMlab.get('', function(err, resM, body) { // Obtenemos la respuesta
    if (!err && body.length > 0 && !body[0].movimientos[idMovimiento-1].financiada) { // No hay errores, hay movimientos y el movimiento no está ya financiado
      for (var i = 0; i < body[0].movimientos.length; i++) { // Buscamos el movimiento
        idValues.push(body[0].movimientos[i].id) // Generamos un array con los valores de cada id de cada movimiento
        if (body[0].movimientos[i].id === idMovimiento) { // Si el movimiento coincide con el que vamos a financiar
          body[0].movimientos[i].financiada = true // Cambiamos su estado a financiado
          indiceOperacion = i // Obtenemos el índice del movimiento a financiar
        }
      }
      movimientoNuevo.id = Math.max(...idValues) + 1 // Calulamos el valor del ID del movimiento nuevo
      movimientoNuevo.fecha = fecha.toISOString().slice(0,10).replace(/-/g, '/') // Obtenemos la fecha del movimiento nuevo
      movimientoNuevo.importe = Math.round(body[0].movimientos[indiceOperacion].importe * (-1) * 100) / 100 // Calculamos el importe del movimiento nuevo
      movimientoNuevo.moneda = body[0].movimientos[indiceOperacion].moneda // Asignamos la misma divisa al movimiento nuevo
      movimientoNuevo.descripcion = 'Financiación ' + body[0].movimientos[indiceOperacion].descripcion // Asignamos la descripción al movimiento nuevo
      // Añadimos el movimiento nuevo al resto de movimentos de la cuenta y actualizamos el saldo de la cuenta
      objMovimientos = body[0].movimientos
      objMovimientos.push(movimientoNuevo) // Movimiento nuevo añadido
      nuevoSaldo = Math.round((body[0].saldo + movimientoNuevo.importe) * 100) / 100 // Calculado el saldo nuevo
      // Persistimos los movimientos y el saldo en mLab
      var cambio = '{"$set":{"movimientos":' + JSON.stringify(objMovimientos) + '}}' // Preparamos el cambio de movimientos
      clienteMlab.put('', JSON.parse(cambio), function(errP, resP, bodyP) { // Acutalizamos los movimientos
        if (!errP) { // Si no hay error
          var cambio = '{"$set":{"saldo":' + nuevoSaldo + '}}' // Preparamos el cambio del saldo
          clienteMlab.put('', JSON.parse(cambio), function(errP, resP, bodyP) { // Actualizamos el saldo de la cuenta
            if (!errP) { // Si no hay error enviamos el saldo actualizado
              res.status(201).send({"saldo": nuevoSaldo})
            } else {
              res.status(409).send({"operación": "POST /cuentas/movimientos/financiaciones", "detalle": "Error mLab"})
            }
          })
        } else {
          res.status(409).send({"operación": "POST /cuentas/movimientos/financiaciones", "detalle": "Error mLab"})
        }
      })
    } else {
      res.status(404).send({"operación": "POST /cuentas/movimientos/financiaciones", "detalle": "Movimiento no encontrado o ya está financiado"})
    }
  })
})

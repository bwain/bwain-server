var mocha = require('mocha')
var chai = require('chai')
var chaiHttp = require('chai-http')
var server = require('../bwain-server')

var should = chai.should()

chai.use(chaiHttp) // configurar chai con módulo HTTP

describe('Test de conectividad', () => {
  it('Google funciona', (done) => {
    chai.request('http://www.google.es')
      .get('/')
      .end((err, res) => {
      res.should.have.status(200)
      done()
    })
  })
})

describe('Test API usuarios', () => {
  it('Raíz OK', (done) => {
    chai.request('http://localhost:3000')
      .get('/bwain-server/v1')
      .end((err, res) => {
      res.should.have.status(200)
      res.body.mensaje.should.be.eql("Bienvenido a BWAIN Server")
      done()
    })
  })
  it('Listado de usuarios', (done) => {
    chai.request('http://localhost:3000')
      .get('/bwain-server/v1/usuarios')
      .end((err, res) => {
        res.should.have.status(200)
        res.body.should.be.a('array')
        for (var i = 0; i < res.body.length; i++) {
          res.body[i].should.have.property('email')
          res.body[i].should.have.property('password')
        }
      done()
      })
  })
})

describe('Test API cuentas', () => {
  it('Listado de cuentas (IBAN)', (done) => {
    chai.request('http://localhost:3000')
      .get('/bwain-server/v1/cuentas')
      .set('idcliente', 1)
      .end((err, res) => {
        res.should.have.status(200)
        res.body[0].should.have.property('iban')
        res.body[0].should.have.property('saldo')
        done()
      })
  })
  it('Listado de movimientos', (done) => {
    chai.request('http://localhost:3000')
      .get('/bwain-server/v1/cuentas/movimientos')
      .set('iban', 'FO35 8652 5888 9782 99')
      .end((err, res) => {
        res.should.have.status(404)
        done()
      })
  })
})
